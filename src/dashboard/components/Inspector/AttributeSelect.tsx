import React, { Component } from "react";

import { fetchDeviceAttributes } from "../api";
import { DeviceConsumer } from "../DevicesProvider";
import DeviceSuggester from "./DeviceSuggester";
import AttributeSuggester from "./AttributeSuggester";

interface Props {
  tangoDB: string;
  device?: string;
  attribute?: string;
  dataFormat?: "scalar" | "spectrum" | "image";
  dataType?: "numeric";
  onSelect?: (device: string | null, attribute: string | null) => void;
}

interface State {
  fetchingAttributes: boolean;
  attributes: Array<{
    name: string;
    datatype: string;
    dataformat: string;
  }>;
}

export default class AttributeSelect extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = { fetchingAttributes: false, attributes: [] };
    this.handleSelectDevice = this.handleSelectDevice.bind(this);
    this.handleSelectAttribute = this.handleSelectAttribute.bind(this);
  }

  public componentDidMount() {
    this.fetchAttributes();
  }

  public componentDidUpdate(prevProps) {
    if (this.props.device !== prevProps.device) {
      this.setState({ attributes: [] });
      this.fetchAttributes();
    }
  }

  public handleSelectDevice(newDevice: string) {
    this.fetchAttributes();
    const { onSelect } = this.props;
    if (onSelect && newDevice) {
      onSelect(newDevice, null);
    }
  }

  public handleSelectAttribute(newAttribute: string) {
    const { onSelect, device } = this.props;
    if (onSelect && device && newAttribute) {
      onSelect(device, newAttribute);
    }
  }

  public render() {
    const { device, attribute } = this.props;
    const attributes = this.filteredAttributes();

    return (
      <DeviceConsumer>
        {({ devices }) => {
          const hasDevice = device != null && device !== "";

          return (
            <div className="AttributeSelect">
              <DeviceSuggester
                deviceName={device}
                devices={devices}
                onSelection={newValue => this.handleSelectDevice(newValue)}
              />
              <AttributeSuggester
                attributeName={attribute}
                attributes={attributes.map(({name}) => name)}
                hasDevice={hasDevice}
                onSelection={newValue => this.handleSelectAttribute(newValue)}
              />
            </div>
          );
        }}
      </DeviceConsumer>
    );
  }

  private filteredAttributes() {
    const numericTypes = [
      "DevDouble",
      "DevFloat",
      "DevLong",
      "DevLong64",
      "DevShort",
      "DevUChar",
      "DevULong",
      "DevULong64",
      "DevUShort"
    ];

    return this.state.attributes.filter(attr => {
      const { dataType, dataFormat } = this.props;

      if (dataFormat === "scalar" && attr.dataformat !== "SCALAR") {
        return false;
      } else if (dataFormat === "spectrum" && attr.dataformat !== "SPECTRUM") {
        return false;
      } else if (dataFormat === "image" && attr.dataformat !== "IMAGE") {
        return false;
      } else if (
        dataType === "numeric" &&
        numericTypes.indexOf(attr.datatype) === -1
      ) {
        return false;
      } else {
        return true;
      }
    });
  }

  private async fetchAttributes() {
    const { device, tangoDB } = this.props;
    if (device) {
      this.setState({ attributes: [], fetchingAttributes: true });
      const attributes = await fetchDeviceAttributes(tangoDB, device);
      this.setState({ attributes, fetchingAttributes: false });
    }
  }
}
